export const colorMap = {
    "default": {
        primary: "#4caf50",//Secondary? 03a9f4
        "primary-dark": "#1b5e20",
        "primary-light": "#A5D6A7",
        secondary: "#ffc107",
        "secondary-accent": "#ffd54f",
        accent: "#CC3363",
        background: "#EEE",
        text: "rgba(0,0,0,0.80)",
        textSecondary: "rgba(0,0,0,0.57)",
        divider: "rgba(0,0,0,.12)",
        shadow: "#777",
        hover: "#CC3363",
        hoverSecondary:"rgba(0,0,0,.88)",
        colorError: "#f44336",
        "color-success": "#00c851",
        "color-input-underline": "#ced4da",
        "default-icon-color": "#000000",
        disabled: "#cfcfcf"
    },
    "dark": {
        primary: "#2e2e2e",
        "primary-dark": "#212121",
        "primary-light": "#BCAAA4",
        secondary: "#37474F",
        "secondary-accent": "#607d8b",
        accent: "#3F729B",
        background: "#4B515D",
        text: "rgba(255,255,255,0.80)",
        textSecondary: "rgba(255,255,255,0.57)",
        divider: "rgba(255,255,255,.12)",
        shadow: "#7d8491",
        hover: "#9933CC",
        colorError: "#b71c1c",
        "color-success": "#00c851",
        "color-input-underline": "#ced4da",
        "default-icon-color": "#000000",
        disabled: "#ddd"
    }
};