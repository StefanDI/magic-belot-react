export const MESSAGE_RECEIVED = 'MESSAGE_RECEIVED';
export const SEND_MESSAGE = 'SEND_MESSAGE';

export const socketMessageReceived = (message) => {
    return{
    type: MESSAGE_RECEIVED,
    payload: {
        type:message.header,
        message: message.message
    }
}};

export const sendMessage  = message => ({
   type:SEND_MESSAGE,
   payload: message
});
