import {colorMap} from "../utils/theme/colors";

export const CHANGE_THEME = "CHANGE_THEME";
const availableThemes = ['default', 'dark'];
export const changeTheme = theme => {
    if(availableThemes[theme])
        throw new Error("Unavailable theme, choose one of " + {...availableThemes})

    return {
        type:CHANGE_THEME,
        payload:colorMap[theme]
    }
};