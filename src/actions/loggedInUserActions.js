export const REQUEST_TO_LOGIN = "REQUEST_LOGIN";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILED = "LOGIN_FAILED";

const LOGIN_URL = "http://localhost:8080/login";

const requestLoginAction = () => ({
    type: REQUEST_TO_LOGIN
});

const loginSuccessAction = (response) => ({
    type: LOGIN_SUCCESS,
    response: response
});

const loginFailedAction = (response) => ({
    type: LOGIN_FAILED,
    response: response
});

export const loginAction = credentials => dispatch => {
    dispatch(requestLoginAction());
    fetch(LOGIN_URL).then((resp) => {
        dispatch(loginSuccessAction(resp));
    }).catch((resp) => {
        dispatch(loginFailedAction(resp));
    })
};