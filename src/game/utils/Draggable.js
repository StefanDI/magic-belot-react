import React, {Component} from 'react';
import './Draggable.css';

class Draggable extends Component {
    constructor(props) {
        super(props);

        this.active = false;
        // this.initialX = 0;
        this.initialY = 0;
        this.xOffset = 0;
        this.yOffset = 0;
    }

    render() {
        return (
            <div ref={node => this.comp = node}
                 onMouseDown={(e) => this.startDrag(e)}
                 onMouseMove={(e) => this.drag(e)}
                 onMouseDownCapture={e => this.dragEnd(e)}
                 className="draggable">
                <div className="back"></div>
                <div className="front"></div>
            </div>
        )
    }

    startDrag(e) {
        if (e.type === "touchstart") {
            this.initialX = e.touches[0].clientX - this.xOffset;
            this.initialY = e.touches[0].clientY - this.yOffset;
        } else {
            this.initialX = e.clientX - this.xOffset;
            this.initialY = e.clientY - this.yOffset;
        }

        // console.log(e.target)
        // //If its the card or card row
        // if (e.target === this.comp) {
        this.active = true;

        // }
    }

    dragEnd(e) {
        this.initialX = this.currentX;
        this.initialY = this.currentY;

        this.active = false;
    }

    drag(e) {
        if (this.active) {

            console.log(e.clientX);
            console.log(e.clientY);

            e.preventDefault();

            if (e.type === "touchmove") {
                this.currentX = e.touches[0].clientX - this.initialX;
                this.currentY = e.touches[0].clientY - this.initialY;
            } else {
                this.currentX = e.clientX - this.initialX;
                this.currentY = e.clientY - this.initialY;
            }



            this.xOffset = this.currentX;
            this.yOffset = this.currentY;
            this.setTranslate(this.currentX, this.currentY, this.comp);

        }
    }

    setTranslate(xPos, yPos, el) {

        el.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
    }

}

export default Draggable