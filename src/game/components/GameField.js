import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import '../css/game-field.css';
import BelotCard from "./BelotCard";
import Hand from "./Hand";
import UniqueIdGenerator from "../../website/util/vanillaJS/UniqueIdGenerator";

const MAX_Y_TRANSLATE = -840;
const MIN_Y_TRANSLATE = 0;

const MAX_Z_TRANSLATE = -300;
const MIN_Z_TRANSLATE = 0;

const MAX_X_ROTATE = 38;
const MIN_X_ROTATE = 0;

class GameField extends Component {

    static DEFAULT_POSITION = {
        translateX: 0,
        translateY: 0,
        translateZ: 0,
        rotateX: 0,
        rotateY: 0,
        rotateZ: 0
    };

    constructor(props) {
        super(props);
        this.state = {
            isActive: false,
            ownCards: [
                {
                    id: UniqueIdGenerator.generate(),
                    suit: 'DIAMONDS',
                    value: 10,
                    position: GameField.DEFAULT_POSITION
                },
                {
                    id: UniqueIdGenerator.generate(),
                    suit: 'HEARTS',
                    value: 'A',
                    position: GameField.DEFAULT_POSITION
                }
            ],
            leftCards: [],
            rightCards: [],
            partnerCards: []
        };

        this.xOffset = 0;
        this.yOffset = 0;
        this.startDrag = this.startDrag.bind(this);
    }

    sendMessage = (msg) => {
        this.clientRef.sendMessage('/belot/', JSON.stringify(msg));

    }

    componentDidMount() {

    }


    render() {
        let dragStyle = {
            transform: `rotateX(${MAX_X_ROTATE}deg)
             translateY(${MAX_Y_TRANSLATE}px)
              translateZ(${MAX_Z_TRANSLATE}px)`
        };
        return (
            <div id="gameField"
                 onMouseDownCapture={this.startDrag}
                 onMouseMoveCapture={(e) => this.dragInProgress(e)}
                 onMouseUpCapture={(e) => this.dragOver(e)}>
                <Hand translateX={700} translateY={1000} translateZ={0} rotateX={0} rotateY={0} rotateZ={0}>
                    {this.state.ownCards.map((card, index) =>
                        <BelotCard cardId={card.id} key={index} cardValue={card.value} cardSuit={card.suit}
                                   position={card.position}/>
                    )}
                    <div ref={(node) => this.dragToConfirm = node}
                         style={{display: this.state.isActive ? 'block' : 'none', ...dragStyle}}
                         className="drag-to-confirm">
                    </div>
                </Hand>


            </div>
        );
    }

    startDrag(e) {

        this.initialX = e.clientX - this.xOffset;
        this.initialY = e.clientY - this.yOffset;

        let card = this.state.ownCards.find((card) => card.id === e.target.id);

        if (card) {
            this.setState({
                isActive: true
            });
            this.dragTarget = card;
            this.lastTransform = {
                ...card.position
            };

            this.lastX = e.clientX;
            this.lastY = e.clientY;
        }

    }

    dragInProgress(e) {
        if (!this.state.isActive) {
            e.preventDefault();
            return;
        }


        let x = e.clientX;
        let y = e.clientY;

        if (y < this.lastY) {
            let newYTranslate = this.lastTransform.translateY - 20 >= MAX_Y_TRANSLATE ? this.lastTransform.translateY - 20 : MAX_Y_TRANSLATE;
            let newZTranslate = this.lastTransform.translateZ - 15 >= MAX_Z_TRANSLATE ? this.lastTransform.translateZ - 15 : MAX_Z_TRANSLATE;
            let newRotateX = this.lastTransform.rotateX + 1 >= MAX_X_ROTATE ? MAX_X_ROTATE : this.lastTransform.rotateX + 1;

            this.lastTransform = {
                ...this.lastTransform,
                translateY: newYTranslate,
                translateZ: newZTranslate,
                rotateX: newRotateX
            };


        } else if (y > this.lastY) {
            let newYTranslate = this.lastTransform.translateY + 15 <= MIN_Y_TRANSLATE ? this.lastTransform.translateY + 15 : MIN_Y_TRANSLATE;
            let newZTranslate = this.lastTransform.translateZ + 15 <= MIN_Z_TRANSLATE ? this.lastTransform.translateZ + 15 : MIN_Z_TRANSLATE;
            let newRotateX = this.lastTransform.rotateX - 1 <= MIN_X_ROTATE ? MIN_X_ROTATE : this.lastTransform.rotateX - 1;
            this.lastTransform = {
                ...this.lastTransform,
                translateY: newYTranslate,
                translateZ: newZTranslate,
                rotateX: newRotateX
            };
        }

        this.lastY = y;
        this.setState(state => {
            let card = state.ownCards.find(card => this.dragTarget.id === card.id);
            card.position = this.lastTransform;
            return card;
        });
    }

    dragOver() {
        if (!this.state.isActive)
            return;

        this.lastX = 0;
        this.lastY = 0;
        this.initialX = 0;
        this.initialY = 0;

        this.setState(state => {
            let card = state.ownCards.find(card => this.dragTarget.id === card.id);
            card.position = this.isCardInsideDropTarget() ? this.lastTransform : GameField.DEFAULT_POSITION;
            return {isActive: false, ...card};
        });
    }

    isCardInsideDropTarget() {
        let cardPosition = document.getElementById(this.dragTarget.id).getBoundingClientRect();
        let dragTargetPosition = ReactDOM.findDOMNode(this.dragToConfirm).getBoundingClientRect();
        const {x, y, height, width} = dragTargetPosition;
        return (cardPosition.x >= x && cardPosition.x <= x + width) &&
            (cardPosition.y + cardPosition.height <= y + height) &&
            (cardPosition.y >= y && cardPosition.y <= y + height)
    }
}

GameField.propTypes = {};
GameField.defaultProps = {};

export default GameField;