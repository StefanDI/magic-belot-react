import React, {Component} from 'react';
import PropTypes from 'prop-types';
import '../css/belot-card.css';

class BelotCard extends Component {

    constructor(props) {
        super(props);

        let img = require(`../images/cards/${this.props.cardValue}-${this.props.cardSuit}.png`);
        let background = require(`../images/cards/card-back.png`);
        this.frontStyle = {
            backgroundImage: `url('${img}')`
        };
        this.backStyle = {
            backgroundImage: `url('${background}')`
        }
    }

    render() {
        const {rotateX, rotateY, rotateZ, translateX, translateY, translateZ} = this.props.position;
        this.cardStyle = {
            transform: `rotateX(${rotateX}deg) rotateY(${rotateY}deg)
            rotateZ(${rotateZ}deg) translateX(${translateX}px)
             translateY(${translateY}px) translateZ(${translateZ}px)`,
            left: this.props.left
        };
        return (
            <div className="belot-card" style={this.cardStyle} >
                <div style={this.props.isVisible ? this.backStyle : this.frontStyle}
                     className={"card-back"}/>
                <div style={!this.props.isVisible ? this.backStyle : this.frontStyle}
                     id={this.props.cardId}
                     className={"card-front"}/>
            </div>
        )
    }

}

BelotCard.propTypes = {
    cardId:PropTypes.any.isRequired,
    cardSuit: PropTypes.oneOf(["CLUBS", "DIAMONDS", "HEARTS", "SPADES"]).isRequired,
    cardValue: PropTypes.oneOf([7, 8, 9, 10, 'J', 'Q', 'K', 'A']).isRequired,
    isVisible: PropTypes.bool,
    position: PropTypes.object
};
BelotCard.defaultProps = {
    isVisible: true,
    position: {
        translateX: 0,
        translateY: 0,
        translateZ: 0,
        rotateX: 0,
        rotateY: 0,
        rotateZ: 0
    }
};

export default BelotCard;