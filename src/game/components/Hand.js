import React from 'react';
import PropTypes from 'prop-types';
import '../css/hand.css';

class Hand extends React.Component {
    constructor(props) {
        super(props);

        let children = [];


        this.state = {
            translateX: this.props.translateX,
            translateY: this.props.translateY,
            translateZ: this.props.translateZ,
            rotateX: this.props.rotateX,
            rotateY: this.props.rotateY,
            rotateZ: this.props.rotateZ,
            children: children
        };

    }

    getStyle() {
        return {
            transform: `rotateX(${this.state.rotateX}deg) rotateY(${this.state.rotateY}deg)
            rotateZ(${this.state.rotateZ}deg) translateX(${this.state.translateX}px)
             translateY(${this.state.translateY}px) translateZ(${this.state.translateZ}px)`
        };
    }

    render(children, fn) {
        let style = this.getStyle();
        return (
            <div className="hand" style={style}>
                {
                    React.Children.map(this.props.children, (child, index) => {
                        let newProps = {
                            ...child.props,
                            left: index * 40
                        };

                        return React.cloneElement(child, newProps);
                    })
                }
            </div>
        )
    }
}

Hand.propTypes = {
    translateX: PropTypes.number.isRequired,
    translateY: PropTypes.number.isRequired,
    translateZ: PropTypes.number.isRequired,
    rotateX: PropTypes.number.isRequired,
    rotateY: PropTypes.number.isRequired,
    rotateZ: PropTypes.number.isRequired
};

Hand.defaultProps = {};


export default Hand;