import React from 'react';
import './App.css';
import Navigation from "./website/components/Navigation";
import LoggedInUser from "./website/util/vanillaJS/LoggedInUser";
import GameField from "./game/components/GameField";
import SockJsClient from "react-stomp";
import {connect} from "react-redux";
import {sendMessage, socketMessageReceived} from "./actions/websocketActions";

if (process.env.NODE_ENV !== 'production') {
    const whyDidYouRender = require('@welldone-software/why-did-you-render/dist/no-classes-transpile/umd/whyDidYouRender.min.js');
    whyDidYouRender(React);
}
const SERVER_URL = 'http://localhost:8080/belot';
const TOPICS = ['/topic/belot-game', "/user/topic/belot-game"];

class App extends React.Component {

    constructor(props) {
        super(props);
    }

    sendMessage = (msg) => {
        setTimeout(() => {
            this.clientRef.sendMessage('/belot/', JSON.stringify(msg));
        }, 2000);
    };

    componentDidMount() {
        // let loginData = new FormData();
        // loginData.append('username', 'stefan');
        // loginData.append('password', '87hspq2');
        // fetch("http://localhost:8080/login", {
        //     method: 'POST',
        //     body: loginData,
        //     credentials: 'include'
        // });
    }

    render() {
        return (
            <div className="App">
                <Navigation/>
                <GameField/>
                {/*<SockJsClient url={SERVER_URL}*/}
                {/*              topics={TOPICS}*/}
                {/*              headers={{*/}
                {/*                  username: 'stefan',*/}
                {/*                  user: 'stefan'*/}
                {/*              }}*/}
                {/*              onConnect={() => this.sendMessage({type: "JOIN_PRIVATE_GAME"})}*/}
                {/*              debug={true}*/}
                {/*              onMessage={(msg) => this.props.dispatch(socketMessageReceived(msg))}*/}
                {/*              ref={(client) => {*/}
                {/*                  this.clientRef = client*/}
                {/*              }}/>*/}
            </div>
        );
    }
}

export default connect()(App);
