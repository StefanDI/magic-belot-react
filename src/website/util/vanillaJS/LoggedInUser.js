/**
 * @type {Object}
 * properties of the user:
 *  id - the id
 *  username - the username
 *  email - the email
 *  isInBelot - true or false, if is playing
 *  belotId - the id of the belot the user is currently in
 *  gameSocket - the socket at which the user communicates with the backend
 * *@author Stefan Ivanov
 */


const LoggedInUser = (function () {
    let observerList = [];

    const loggedInUser = {
        isPresent: false,
        getLoggedInUser: getLoggedInUser,
        logout: logout,
        gameSocket: "",
        isInBelot: false,
        belotId: "",
        lastMessage: "",
        theme: "default"
    };

    function getLoggedInUser() {
        loggedInUser.username = "Stefan";
        loggedInUser.theme = "dark";
        loggedInUser.isPresent = true;
        // $.get({
        //     url: ServerConstants.WHOLE_URL + "/users/me",
        //     complete: (resp) => {
        //         if (resp.responseText) {
        //             let userFromBackend = JSON.parse(resp.responseText);
        //             Object.keys(userFromBackend).forEach(prop => loggedInUser[prop] = userFromBackend[prop]);
        //             loggedInUser.isPresent = true;
        //             //Once we log in we immediately open a web socket and subscribe to belot
        //             loggedInUser.gameSocket = new WebSocket("/belot");
        //             if (!loggedInUser.gameSocket.isConnected)
        //                 loggedInUser.gameSocket.connect(() => {
        //
        //                     loggedInUser.gameSocket.subscribe("/user/topic/belot-game", handleResponse);
        //                     loggedInUser.gameSocket.subscribe("/topic/belot-game", handleResponse);
        //
        //                     //TODO: remove this when testing is over
        //                     joinGame();
        //                 });
        //
        //         }
        //     }
        // });
    }

    function logout() {
        // loggedInUser.isPresent = false;
        // if (loggedInUser.gameSocket.isConnected) {
        //     loggedInUser.gameSocket.disconnect();
        //     loggedInUser.gameSocket.unsubscribeAll();
        // }
    }





    return {
        getLoggedInUser:getLoggedInUser,
        loggedInUser: loggedInUser
    };
})();

export default LoggedInUser;