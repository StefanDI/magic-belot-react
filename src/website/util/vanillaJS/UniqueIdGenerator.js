let UniqueIdGenerator = (function () {

    let lastId = 0;

    return {
        generate: function (prefix = 'id') {
            lastId++;
            return `${prefix}${lastId}`;
        }
    }

})();


export default UniqueIdGenerator;