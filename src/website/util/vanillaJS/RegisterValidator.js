export default (function () {

    const USERNAME_MIN_SIZE = 3;
    const USERNAME_MAX_SIZE = 16;
    const USERNAME_VALIDATION_REGEX = "^[a-zA-Z0-9_\\-]+$";
    const PASSWORD_MIN_LENGTH = 6;
    const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


    let pass;
    let confirmPass;

    function validateUsername(username) {
        if (!username || username.length === 0) {
            return "Username cannot be empty";
        } else if (username.length < USERNAME_MIN_SIZE) {
            return `Username cannot be less than ${USERNAME_MIN_SIZE} characters`;
        } else if (username.length > USERNAME_MAX_SIZE) {
            return `Username cannot be more than ${USERNAME_MAX_SIZE} characters`;
        } else if (!username.match(USERNAME_VALIDATION_REGEX)) {
            return "Username contains illegal characters";
        }

        return true;
    }


    function validateEmail(email) {
        if (!email || email.length === 0) {
            return "Email cannot be empty";
        } else if (!validateEmailRegex(email)) {
            return "Invalid Email";
        }

        return true;
    }

    function validatePassword(password) {
        if (!password)
            return "Password cannot be empty";
        else if ((pass && password !== pass)
            || (confirmPass && password !== confirmPass)) {
            return "Passwords dont match";
        } else if (password.length < PASSWORD_MIN_LENGTH) {
            return `Password cannot be less than ${PASSWORD_MIN_LENGTH} symbols`;
        }

        return true;
    }


    function validateEmailRegex(email) {
        return EMAIL_REGEX.test(String(email).toLowerCase());
    }


    return {
        validateUsername: (username) => {
            return validateUsername(username);
        },
        validateEmail: (email) => {
            return validateEmail(email);
        },
        validatePassword: (password) => {
            pass = password;
            return validatePassword(password);
        },
        validateConfirmPassword: (password) => {
            confirmPass = password;
            return validatePassword(password);
        },
    }
})();