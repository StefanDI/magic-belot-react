import React from 'react';
import ReactDOM from 'react-dom';
import "../../css/util/loading-style.css";


const Loadable = (LoadingComponent) => {

    return class Loadable extends React.PureComponent {

        constructor(props) {
            super(props);

            this.nodeRef = React.createRef();

            this.state = {
                width: 0,
                height: 0
            };
        }

        /**
         * Make the overlay as big as the element that is passed
         */
        componentDidMount() {
            let el = ReactDOM.findDOMNode(this.nodeRef.current);
            let rect = el.getBoundingClientRect();
            this.setState({
                width: rect.width,
                height: rect.height
            });
        }

        render() {
            const style = {
                width: this.state.width,
                height: this.state.height
            };

            let props = {...this.props};
            delete props.isLoading;
            return (
                this.props.isLoading ?
                    <div className="loading-container">
                        <div className="loading" style={style}>
                            <div className="circle circle-one"/>
                            <div className="circle circle-two"/>
                            <div className="circle circle-three"/>
                        </div>
                        <LoadingComponent
                            ref={this.nodeRef}
                            {...props}>
                        </LoadingComponent>
                    </div>
                    ://OR
                    <LoadingComponent
                        ref={this.nodeRef}
                        {...props}>
                    </LoadingComponent>
            )
        }
    }
};

Loadable.whyDidYouRender = true;
export default Loadable;