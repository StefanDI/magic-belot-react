import React from 'react';
import PropTypes from 'prop-types';
import '../../css/util/horizontal.css'

const Horizontal = (props) => {
    let style = {};
    if(props.alignRight)
        style.justifyContent = "flex-end";

    return (<div className="horizontal" style={style}>
        {props.children}
    </div>);
};
Horizontal.propTypes = {
    alignRight:PropTypes.bool
}
export default Horizontal


