import React, {Component} from 'react';
import PropTypes from 'prop-types';
import '../../css/basic/modal.css';
import UniqueIdGenerator from "../../util/vanillaJS/UniqueIdGenerator";
import {connect} from "react-redux";
import {colorMap} from "../../../utils/theme/colors";

/**
 * Its simply a modal dialog no content no default close button
 *
 */
class Modal extends Component {

    // static whyDidYouRender = true;

    constructor(props) {
        super(props);

        this.state = {
            isOpen: this.props.isOpen
        };

        this.modal = React.createRef();

        this.id = UniqueIdGenerator.generate();
    }

    componentDidMount() {
        this.modal.onclick = (event) => {
            if (event.target === this.modal &&
                this.props.isClosable &&
                this.state.isOpen) {

                this.closeModal();
            }
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.isOpen !== this.props.isOpen &&
            this.state.isOpen !== this.props.isOpen) {
            this.setState({isOpen: this.props.isOpen});
        }
    }

    closeModal() {
        if (this.state.isOpen) {
            this.setState({isOpen: false});
            this.props.toggleChange(false);
        }
    }

    render() {
        let modalStyles = this.extractModalStyles();
        return (
            <div id={this.id}
                 ref={node => this.modal = node}
                 className={this.state.isOpen ? "modal modal-shown" : "modal modal-hidden"}>
                <div
                    className="modal-body"
                    style={modalStyles}>
                    {this.props.children}
                </div>
            </div>
        )
    }

    extractModalStyles() {
        return {
            backgroundColor:this.props.theme.background,
            width: this.props.sizeInPercent + "%"
        }
    }
}

Modal.propTypes = {
    toggleChange: PropTypes.func.isRequired,
    isOpen: PropTypes.bool,
    isClosable: PropTypes.bool,
    sizeInPercent: PropTypes.number
};
Modal.defaultProps = {
    isOpen: false,
    isClosable: true,
    sizeInPercent: 80,
    theme:colorMap["default"]
};

const mapStateToProps = ({themeReducer}) => themeReducer;
export default connect(mapStateToProps)(Modal);