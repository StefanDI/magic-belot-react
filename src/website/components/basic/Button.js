import React from 'react';
import PropTypes from 'prop-types';
import '../../css/basic/button.css';
import {colorMap} from "../../../utils/theme/colors";
import {connect} from "react-redux";


const Button = (props) => {
    let style = {
        background: props.color === "primary" ? props.theme.primary : props.theme.secondary,
        color: props.color === "primary" ? props.theme.text : props.theme.textSecondary
    };
    // delete props.dispatch; //TODO: There must be better way
    let propsWithoutDispatch = {...props};
    delete propsWithoutDispatch.dispatch;
    return <button type="button" style={style} className={props.btntype + " default " + props.btnclass} {...propsWithoutDispatch}>
        {props.text || props.children}
    </button>;
};
Button.propTypes = {
    btnclass: PropTypes.string,
    btntype: PropTypes.oneOf(["button", "text", "outline"]),
    color:PropTypes.oneOf(["primary", "secondary"])
};
Button.defaultProps = {
    btntype: "button",
    btnclass: "",
    color:"primary",
    theme:colorMap['default']
};
const mapStateToProps = ({themeReducer}) => themeReducer;
export default connect(mapStateToProps)(Button);