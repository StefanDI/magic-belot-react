import React, {Component} from 'react';
import PropTypes from 'prop-types';
import '../../css/basic/anchor.css';
import {connect} from "react-redux";
import {colorMap} from "../../../utils/theme/colors";

/**
 * <a> element
 */
class Anchor extends Component {
    constructor(props) {
        super(props);
        const {theme} = this.props;
        this.state = {
            color: this.props.type === 'primary' ? theme.text : theme.textSecondary,
            hover: this.props.type === 'primary' ? theme.hover : theme.hoverSecondary
        };
    }

    static getDerivedStateFromProps(nextProps, currentState) {
        const {theme} = nextProps;

        return {
            color: nextProps.type === 'primary' ? theme.text : theme["text-secondary"],
            hover: nextProps.type === 'primary' ? theme.hover : theme.hoverSecondary
        }
    }


    render() {
        let style = {
            color: this.state.color
        };
        return (
            <a id={this.props.id} href={this.props.href} className={this.props.className}
               onClickCapture={this.props.onclick}
               style={style} onMouseOverCapture={() => this.hover()}
               onMouseOutCapture={() => this.hoverOut()}>
                {this.props.text}
            </a>
        )
    }

    hover() {
        this.setState({
            color: this.state.hover
        });
    }

    hoverOut() {
        this.setState({
            color: this.state.color
        });
    }

}

Anchor.propTypes = {
    type: PropTypes.oneOf(["primary", "secondary"]),
    className: PropTypes.string,
    id: PropTypes.string,
    href: PropTypes.string,
    text: PropTypes.any,
    onclick: PropTypes.func
};

Anchor.defaultProps = {
    type: "primary",
    className: "anchor",
    href: "#",
    text: "",
    theme: colorMap['default']
};


const mapStateToProps = ({themeReducer}) => themeReducer;
export default connect(mapStateToProps)(Anchor);