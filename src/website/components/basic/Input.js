import React from 'react';
import PropTypes from "prop-types";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import "../../css/basic/input.scss";
import {colorMap} from "../../../utils/theme/colors";
import {connect} from "react-redux";

/**
 * <input/>
 */
class Input extends React.PureComponent {

    // static whyDidYouRender = true;

    constructor(props) {
        super(props);

        this.state = {
            isFocused: false,
            isHovered: false,
            isError: this.props.isError,
            textUnder: this.props.textUnder,
            shouldGoTop: this.props.placeholder !== "" || this.props.inputText !== "",
            shouldChangeColor: false,
            value: this.props.inputText
        };

        this.onInputChange = this.onInputChange.bind(this);
    }

    static getDerivedStateFromProps(nextProps) {

        //shouldGoTop: nextProps.placeholder !== "" || nextProps.inputText !== ""
        return {
            isError: nextProps.isError,
            textUnder: nextProps.textUnder
        }
    }

    onInputChange(e) {
        this.setState({value: e.target.value});
        if (this.props.onInputChange)
            this.props.onInputChange(e);
    }

    focused() {

        this.setState({
            isFocused: true,
            shouldGoTop: true,
            shouldChangeColor: true
        });
    }

    blurred() {
        let shouldGoTop = this.input.value !== "";

        this.setState({
            shouldChangeColor: false,
            isFocused: false,
            shouldGoTop: shouldGoTop
        });

        if (this.props.onBlur)
            this.props.onBlur();
    }


    render() {
        this.extractStyles();

        //if there is a icon from props, render it, else   ""
        let Icon = this.props.icon ?
            <FontAwesomeIcon style={{color: this.textColor}} icon={this.props.icon} transform={"grow-5"}/> : "";

        let floatingLabelClass = this.extractClassList();
        let floatingLabelStyle = this.extractFloatingLabelStyle();

        let inputStyles = this.extractInputStyles();
        let textUnderStyle = this.extractTextUnderStyle();


        return (
            <div onClickCapture={() => this.input.focus()}
                 className={this.state.isFocused ? "input-group focused" : "input-group"}>
                {Icon}
                <label
                    className={floatingLabelClass}
                    style={floatingLabelStyle}
                    htmlFor={this.props.id}>{this.props.label}</label>

                <input id={this.props.id}
                       style={inputStyles}
                       ref={el => this.input = el}
                       value={this.state.value}
                       onChange={this.onInputChange}
                       type={this.props.type}
                       placeholder={this.state.shouldGoTop ? this.props.placeholder : ""}
                       onFocus={() => this.focused()}
                       onBlur={() => this.blurred()}

                />
                <label style={textUnderStyle} className={"text-under"}>
                    {this.state.textUnder}
                </label>
            </div>
        )
    }

    /**
     * Everything below is style related
     */
    extractClassList() {
        let classList = "floating-label";
        if (this.state.shouldGoTop)
            classList += " focused ";
        return classList;
    }

    extractInputStyles() {
        let style = {
            color: this.textColor
        };
        if (this.state.isFocused) {
            style.textIndent = this.leftIndent;
            style.borderBottomColor = this.borderColor;
        } else {
            style.textIndent = this.leftIndent;
            if (this.state.isError)
                style.borderBottomColor = this.borderColor;
        }
        return style;

    }

    extractFloatingLabelStyle() {
        return {
            left: this.leftIndent,
            color: this.state.isError ? this.textUnderColor : this.textColor
        }
    }

    extractStyles() {
        const {theme} = this.props;
        this.leftIndent = this.props.icon ? 28 : 8; //if there is an icon indent the text and the label left to fit the icon

        this.textColor = this.state.isFocused ? theme.accent : theme.text;
        this.textUnderColor = theme.text;
        this.borderColor = theme.accent;

        if (this.state.isError) {
            this.borderColor = theme.colorError;
            this.textColor = theme.colorError;
            this.textUnderColor = theme.colorError;
        }

    }

    extractTextUnderStyle() {
        return {
            color: this.textUnderColor
        }
    }

}

Input.propTypes = {
    id: PropTypes.string,
    type: PropTypes.oneOf(["password", "text", "email", "number", "checkbox", "radio", "search"]),
    placeholder: PropTypes.string,
    isError: PropTypes.bool,
    textUnder: PropTypes.string,
    label: PropTypes.string,
    inputText: PropTypes.string,
    onBlur: PropTypes.func,
    onInputChange: PropTypes.func
};
Input.defaultProps = {
    label: "Label",
    type: "text",
    placeholder: "",
    isError: false,
    textUnder: "",
    inputText: "",
    theme:colorMap['default']
};
const mapStateToProps = ({themeReducer}) => themeReducer;
export default connect(mapStateToProps,null,null, {forwardRef:true})(Input);