import React from 'react';
import LoggedInUser from "../util/vanillaJS/LoggedInUser";
import Anchor from "./basic/Anchor";
import RegisterModalContainer from "./authentication/register/RegisterModalContainer";
import LoginModal from "./authentication/login/LoginModal";
import '../css/navigation.css';
import {connect} from "react-redux";
import {changeTheme} from "../../actions/themeActions";
import {connectWebsocket} from "../../actions/websocketActions";

class Navigation extends React.PureComponent {

    static whyDidYouRender = true;

    constructor(props) {
        super(props);

        this.state = {
            isLoggedInUser: false,
            isLoginModalOpen: false,
            isRegisterModalOpen: false
        };
    }

    componentDidMount() {
        // let formData = new FormData();
        // formData.append("email", "a582hs@gmail.com");
        // formData.append("username", "stefan");
        // formData.append("password", "ivanov");
        // formData.append("confirmPassword", "ivanov");
        // let prom = fetch('http://localhost:8080/register',
        //     {
        //         headers: {
        //             "Access-Control-Request-Headers": "auth_token",
        //             "Access-Control-Request-Method": "POST",
        //             'Access-Control-Allow-Origin': '*'
        //         },
        //         mode:'cors',
        //         method: "post",
        //         body: formData
        //     });
        // console.log(prom)
        // prom.then(resp => resp.json())
        //     .then(data => console.log(data));

        // this.props.dispatch(connectWebsocket("http://127.0.0.1:8080/belot"));
    }

    static getDerivedStateFromProps(nextProps) {
        return null;
    }

    render() {
        let needLoginStyle = {display: this.state.isLoggedInUser ? "block" : 'none'};
        let noNeedLoginStyle = {display: !this.state.isLoggedInUser ? "block" : 'none'};

        let navStyle = {
            background: this.props.theme.primary,
            color: this.props.theme.text
        };
        return (
            <nav>
                <LoginModal isOpen={this.state.isLoginModalOpen}
                            toggleChange={(isOpen) => this.setState({isLoginModalOpen: isOpen})}/>
                <RegisterModalContainer isOpen={this.state.isRegisterModalOpen}
                                        toggleChange={(isOpen) => this.setState({isRegisterModalOpen: isOpen})}/>

                <ul style={navStyle} className="navigation">
                    <li style={noNeedLoginStyle} className="nav-item">
                        <Anchor className="nav-link" href="#" id="register" text="Register" onclick={() => {
                            this.setState({isRegisterModalOpen: true})
                            this.props.dispatch(changeTheme('dark'));
                        }}/>
                    </li>
                    <li style={noNeedLoginStyle} className="nav-item">
                        <Anchor className="nav-link" href="#" text="Login" onclick={() => {
                            this.setState({isLoginModalOpen: true})
                        }}/>
                    </li>
                    <li style={needLoginStyle} className="nav-item">
                        <span className="nav-link ">
                            <p className="mr-2 app-text">
                                {LoggedInUser.loggedInUser.username}
                            </p>

                            <img className="user-image-small"
                                 src="https://www.pngfind.com/pngs/m/341-3416003_no-avatar-pic-unknown-person-png-transparent-png.png"
                                 alt="profile"/>
                         </span>
                    </li>
                </ul>
            </nav>

        )
    }

}

Navigation.propTypes = {};

Navigation.defaultProps = {};

const mapStateToProps = ({themeReducer}) => themeReducer;
export default connect(mapStateToProps)(Navigation);