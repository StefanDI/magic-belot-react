import React from 'react';
import Input from "../../basic/Input";
import Modal from "../../basic/Modal";
import PropTypes from 'prop-types';
import {faEnvelope, faKey, faUser} from "@fortawesome/free-solid-svg-icons";
import Loadable from "../../../util/components/Loadable";
import RegisterValidator from "../../../util/vanillaJS/RegisterValidator";
import Button from "../../basic/Button";
import Horizontal from "../../../util/components/Horizontal";


//TODO: Bind password and confirm password validation state
class RegisterModal extends React.Component {

    // static whyDidYouRender = true;

    constructor(props) {
        super(props);

        const username = {
            helperText: "Username should be between 3 and 16 symbols",
            errorText: "",
            value: "",
            isLoading: false,
            isValidated: true
        };

        const email = {...username};
        const password = {...username};
        const confirmPassword = {...username};
        email.helperText = "Example@gmail.com";
        password.helperText = "Password must be 6 symbols";
        confirmPassword.helperText = "";

        this.state = {
            username: username,
            email: email,
            password: password,
            confirmPassword: confirmPassword
        };

        this.usernameInput = Loadable(Input);
        this.emailInput = Loadable(Input);

        this.validateField = this.validateField.bind(this);
        this.submitForm = this.submitForm.bind(this);

    }

    inputChanged(input, e) {
        let state = {...this.state};
        state[input].value = e.target.value;
        state[input].isValidated = false;

        if (this.state[input].value !== state[input].value) {
            this.setState(state);
        }
    }


    render() {

        const UsernameInput = this.usernameInput;
        const EmailInput = this.emailInput;

        return (
            <Modal isOpen={this.props.isOpen} toggleChange={this.props.toggleChange}>
                <UsernameInput
                    key={"username_input"}
                    id={"username-input"}
                    displayName={"UsernameInput"}
                    label={"Username"}
                    isError={this.state.username.errorText !== ''}
                    textUnder={this.state.username.errorText || this.state.username.helperText}
                    onBlur={(e) => this.validateField("username", e)}
                    onInputChange={(e) => this.inputChanged("username", e)}
                    inputText={this.state.username.value}
                    isLoading={this.state.username.isLoading}
                    icon={faUser}/>
                <EmailInput key={"email_input"}
                            id={"email-input"}
                            displayName={"EmailInput"}
                            label={"Email"}
                            isError={this.state.email.errorText !== ''}
                            textUnder={this.state.email.errorText || this.state.email.helperText}
                            onBlur={() => this.validateField("email")}
                            onInputChange={(e) => this.inputChanged("email", e)}
                            inputText={this.state.email.value}
                            isLoading={this.state.email.isLoading}
                            icon={faEnvelope}/>

                <Input key={"password_input"}
                       id={"password-input"}
                       displayName={"PasswordInput"}
                       label={"Password"}
                       isError={this.state.password.errorText !== ''}
                       textUnder={this.state.password.errorText || this.state.password.helperText}
                       onBlur={() => this.validateField("password")}
                       onInputChange={(e) => this.inputChanged("password", e)}
                       inputText={this.state.password.value}
                       icon={faKey}
                />

                <Input
                    id={"confirm-password-input"}
                    key={"confirm-password-input"}
                    displayName={"ConfirmPasswordInput"}
                    label={"Confirm password"}
                    isError={this.state.confirmPassword.errorText !== ''}
                    textUnder={this.state.confirmPassword.errorText || this.state.confirmPassword.helperText}
                    onBlur={() => this.validateField("confirmPassword")}
                    onInputChange={(e) => this.inputChanged("confirmPassword", e)}
                    inputText={this.state.confirmPassword.value}
                    icon={faKey}
                />

                <Horizontal alignRight>
                    <Button color={"secondary"} text={"cancel"} onClickCapture={() => this.props.toggleChange()}/>
                    <Button text={"register"} onClick={this.submitForm}/>
                </Horizontal>
            </Modal>
        )
    }

    submitForm() {
        this.props.submitRegisterForm("TODO serialize form data")
    }

    validateField(field) {
        if (this.state[field].isValidated)
            return;

        let functionName = `validate${field.charAt(0).toUpperCase() + field.slice(1)}`;

        let isValid = RegisterValidator[functionName](this.state[field].value);
        let state = {...this.state};
        let fieldObj = {...this.state[field]};
        fieldObj.isValidated = true;
        if (isValid !== true) {
            if (fieldObj.errorText !== isValid) {
                fieldObj.errorText = isValid;
                state[field] = fieldObj;
                this.setState(state);
            }
        } else {
            if (field === 'username' || field === 'email') {
                fieldObj.isLoading = true;
                state[field] = fieldObj;
                this.setState(state);
                this.props.isAvailable(field, this.state[field].value)
                    .then((resp) => {
                        //TODO: Correct back end response handling
                        fieldObj.errorText = resp.isError ? resp.message : "";
                        fieldObj.helperText = resp.isError ? "" : resp.message;
                        fieldObj.isLoading = false;

                        state[field] = fieldObj;
                        this.setState(state);
                    })
            } else { //Password or confirm password
                this.updatePasswordFields();
            }
        }
    }

    updatePasswordFields() {
        let password = {...this.state.password};
        let confirmPassword = {...this.state.confirmPassword};
        if (this.state.password.value !== this.state.confirmPassword.value) {
            password.errorText = "Passwords dont match";
            confirmPassword.errorText = "Passwords dont match";
        } else {
            password.errorText = "";
            password.helperText = "";
            confirmPassword.errorText = "";
        }

        this.setState({
            password: password,
            confirmPassword: confirmPassword
        })
    }
}

RegisterModal.propTypes = {
    isAvailable: PropTypes.func.isRequired,
    submitRegisterForm: PropTypes.func.isRequired
};

RegisterModal.defaultProps = {};

export default RegisterModal;