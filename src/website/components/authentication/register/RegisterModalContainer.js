import React from 'react';
import RegisterModal from "./RegisterModal";

class RegisterModalContainer extends React.Component {

    // static whyDidYouRender = true;

    constructor(props) {
        super(props);
        this.isAvailable = this.isAvailable.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    isUsernameTaken(username) {
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                resolve(Math.round(Math.random()) % 2 === 0);
            }, 700);
        });
    }

    isEmailTaken(email) {
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                resolve(Math.round(Math.random()) % 2 === 0);
            }, 700);
        });
    }

    render() {
        return (
            <RegisterModal
                isAvailable={this.isAvailable}
                submitRegisterForm={this.submitForm}
                {...this.props}/>
        )
    }

    isAvailable(field, value) {
       if(field.toLowerCase() === 'username')
           return this.isUsernameTaken(value);
       else
           return this.isEmailTaken(value);
    }

    submitForm(formData) {
        //TODO: Submit to backend
    }
}

export default RegisterModalContainer;