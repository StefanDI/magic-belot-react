import React from 'react';
import Modal from "../../basic/Modal";
import Input from "../../basic/Input";
import Button from "../../basic/Button";
import {faKey, faUser} from "@fortawesome/free-solid-svg-icons";

import Horizontal from "../../../util/components/Horizontal";
import Loadable from "../../../util/components/Loadable";

import '../../../css/authentication/login.css'
import LoggedInUser from "../../../util/vanillaJS/LoggedInUser";

class LoginModal extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            username: {
                value: "",
                error: ""
            },
            password: {
                value: "",
                error: ""
            }
        };

        this.form = React.createRef();
        this.login = this.login.bind(this);
        this.wrapper = Loadable('div');
    }

    login() {
        if (this.validate()) {
            this.setState({isLoading: true});
            setTimeout(() => {
                this.setState({
                    isLoading: false
                });

                LoggedInUser.getLoggedInUser();
                this.props.toggleChange();
            }, 1000)
            //TODO: Submit form
        }

    }

    validate() {
        let state = {...this.state};
        state.username.error = "";
        state.password.error = "";

        let isValid = true;
        if (state.username.value.trim() === "") {
            state.username.error = "This field is required";
            isValid = false;
        }
        if (state.password.value.trim() === "") {
            state.password.error = "This field is required";
            isValid = false;
        }

        if (state !== this.state)
            this.setState(state);

        return isValid;
    }

    inputChanged(input, e) {
        let state = {...this.state};
        state[input].value = e.target.value;

        if (this.state[input].value !== state[input].value) {
            this.setState(state);
        }
    }

    render() {
        let Wrapper = this.wrapper;
        return (<Modal sizeInPercent={50} isOpen={this.props.isOpen} toggleChange={this.props.toggleChange}>
            <Wrapper className="card app-background" isLoading={this.state.isLoading}>
                <Input label={"Username"}
                       icon={faUser}
                       isError={this.state.username.error !== ''}
                       textUnder={this.state.username.error}
                       onInputChange={(e) => this.inputChanged("username", e)}
                       inputText={this.state.username.value}/>
                <Input
                    label={"Password"}
                    icon={faKey}
                    isError={this.state.password.error !== ''}
                    textUnder={this.state.password.error}
                    onInputChange={(e) => this.inputChanged("password", e)}
                    inputText={this.state.password.value}
                />
                <Horizontal alignRight>
                    <Button btntype={"button"} color={"secondary"} onClickCapture={() => this.props.toggleChange()}
                            text={"Cancel"}/>
                    <Button btntype={"button"} onClickCapture={this.login}>Submit</Button>
                </Horizontal>
            </Wrapper>
        </Modal>)
    }

};

export default LoginModal