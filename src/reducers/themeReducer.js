import {colorMap} from "../utils/theme/colors";

const defaultState = colorMap['default'];

export const themeReducer = (state = defaultState, action) => ({
    theme: action.payload || defaultState //TODO: This is not right, should be with switch case
});