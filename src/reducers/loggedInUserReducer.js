import {LOGIN_FAILED, LOGIN_SUCCESS, REQUEST_TO_LOGIN} from "../actions/loggedInUserActions";

const defaultState = {
    isLoading:false,
    loginData:{},
    errorMessage:{}
};
export const loggedInUserReducer = (state = defaultState, action) => {
    switch (action.type) {
        case REQUEST_TO_LOGIN:
            return {
                isLoading: true
            };
        case LOGIN_SUCCESS:
            return {
                isLoading: false,
                loginData: action.response//TODO:Map the logged in user here
            };
        case LOGIN_FAILED:
            return {
                isLoading: false,
                errorMessage: action.response//TODO: map the error
            };
        default:
            return state;
    }
};
