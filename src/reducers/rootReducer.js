import {combineReducers} from "redux";
import {loggedInUserReducer} from "./loggedInUserReducer";
import {themeReducer} from "./themeReducer";

export default combineReducers({loggedInUserReducer, themeReducer})