import {MESSAGE_RECEIVED, SEND_MESSAGE} from "../actions/websocketActions";

export const socketMessageReducer = (state = {
    message: null
}, action) => {
    switch (action.type) {
        case MESSAGE_RECEIVED:
            return {
                type:action.payload.type,
                message: action.payload.message
            };

        case SEND_MESSAGE :
            return {
                message: action.payload
            };
        default:
            return state;
    }
};